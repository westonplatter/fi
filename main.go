package main

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"

	importers "bitbucket.org/westonplatter/fi/importers"
	models "bitbucket.org/westonplatter/fi/models"
)

func (s *Server) simulateNotesUpload() {
	filename := "test.csv"
	s.processNotesFile(filename)
}

func (s *Server) simualteLoanUpload() {
	filename := "lctest.csv"
	s.processLoanDataFile(filename)
}

func (s *Server) processLoanDataFile(filename string) {
	importers.ImportLendingClubHistoricalLoanCSV(s.db, filename)
}

func (s *Server) processNotesFile(filename string) {
	importers.ImportSecondaryMarketPlaceCSV(s.db, filename)
}

func main() {

	// configs
	server := &Server{}

	// app env configs
	appEnv := os.Getenv("APP_ENV")
	if appEnv == "" {
		appEnv = "development"
	}
	fmt.Println("APP_ENV=" + appEnv)

	// db connection info
	pgUser := os.Getenv("POSTGRES_USER")
	pgPassword := os.Getenv("POSTGRES_PASSWORD")
	pgHost := os.Getenv("POSTGRES_PORT_5432_TCP_ADDR")
	pgPort := os.Getenv("POSTGRES_PORT_5432_TCP_PORT")
	pgDatabase := "fi_" + appEnv

	// "postgres://pqgotest:password@localhost/pqgotest?sslmode=verify-full"
	dbConnectionString := "postgres://" + pgUser + ":" + pgPassword + "@" + pgHost + ":" + pgPort + "/" + pgDatabase + "?sslmode=disable"
	db, err := gorm.Open("postgres", dbConnectionString)
	PanicErr(err)
	defer db.Close()

	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(25)
	server.db = db

	// Migrate models
	db.AutoMigrate(&models.Note{}, &models.User{}, &models.Loan{})

	// Indexes Note
	db.Model(&models.Note{}).AddUniqueIndex("idx_notes_id", "id")
	db.Model(&models.Note{}).AddUniqueIndex("idx_notes_note_id", "note_id")
	db.Model(&models.Note{}).AddIndex("idx_notes_fi_state", "fi_state")
	db.Model(&models.Note{}).AddIndex("idx_notes_loan_id", "loan_id")
	db.Model(&models.Note{}).AddIndex("idx_notes_market_state", "market_state")
	db.Model(&models.Note{}).AddIndex("idx_notes_order_id", "order_id")
	db.Model(&models.Note{}).AddIndex("idx_notes_interest_rate", "interest_rate")
	db.Model(&models.Note{}).AddIndex("idx_notes_ytm", "ytm")
	db.Model(&models.Note{}).AddIndex("idx_notes_never_late", "never_late")

	// Indexes Loan
	db.Model(&models.Loan{}).AddUniqueIndex("idx_loans_id", "id")
	db.Model(&models.Loan{}).AddUniqueIndex("idx_loans_loan_id", "loan_id")
	db.Model(&models.Loan{}).AddIndex("idx_loans_di", "dti")

	// http
	router := mux.NewRouter()
	router.HandleFunc("/api/notes/upload", server.notesUploadHandler)
	router.HandleFunc("/api/notes/search", server.apiNotesSearch)
	router.HandleFunc("/api/loans/upload", server.loansUploadHandler)
	router.HandleFunc("/api/loans/search", server.apiLoansSearch)

	router.PathPrefix("/").Handler(http.FileServer(http.Dir("./static/")))
	http.Handle("/", Log(router))

	// dev env
	//server.simualteLoanUpload()
	//server.simulateNotesUpload()

	// http listen. ie, run the web app
	port := os.Getenv("PORT")
	if port == "" {
		port = "3000"
	}
	port = ":" + port
	fmt.Println("Fi Listening on 127.0.0.1" + port)
	http.ListenAndServe(port, nil)
}

func (server *Server) loansUploadHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile("file2")
	CheckErr(err)

	f, err := os.OpenFile("./tmp/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	CheckErr(err)
	io.Copy(f, file)

	go func() {
		server.processLoanDataFile(handler.Filename)
	}()

	file.Close()
	http.Redirect(w, r, "/", 301)
}

func (server *Server) notesUploadHandler(w http.ResponseWriter, r *http.Request) {

	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile("file")
	CheckErr(err)

	f, err := os.OpenFile("./tmp/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	CheckErr(err)
	io.Copy(f, file)

	// process file
	// @todo push processNotesFile to a real background worker
	go func() {
		server.processNotesFile(handler.Filename)
	}()

	file.Close()
	http.Redirect(w, r, "/", 301)
}

func (server *Server) apiNotesSearch(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	var notesWhere string
	var value string
	var q string
	var qs []string

	// @todo genericize form extraction

	if value = r.FormValue("interest_rate_gt"); value != "" {
		q = "notes.interest_rate > " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("interest_rate_lt"); value != "" {
		q = "notes.interest_rate < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("ytm_gt"); value != "" {
		q = "notes.ytm > " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("ytm_lt"); value != "" {
		q = "notes.ytm < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("discount_gt"); value != "" {
		q = "notes.discount > " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("discount_lt"); value != "" {
		q = "notes.discount < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("amount_gt"); value != "" {
		q = "notes.amount > " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("amount_lt"); value != "" {
		q = "notes.amount < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("never_late"); value != "" {
		q = "notes.never_late = '" + value + "'"
		qs = append(qs, q)
	}

	if value = r.FormValue("loan_class"); value != "" {
		q = "notes.loan_class in ("
		loan_classes := strings.Split(value, ",")

		for i, c := range loan_classes {
			q = q + "'" + strings.ToUpper(c) + "'"
			if i != len(loan_classes)-1 {
				q = q + ","
			}
		}
		q = q + ")"
		qs = append(qs, q)
	}

	if value = r.FormValue("dti_gt"); value != "" {
		q = "loans.dti > " + value
		qs = append(qs, q)
	}
	if value = r.FormValue("dti_lt"); value != "" {
		q = " loans.dti < " + value
		notesWhere = notesWhere + " AND " + q
	}

	if value = r.FormValue("revolving_utilization_gt"); value != "" {
		q = "loans.revolving_utilization > " + value
		qs = append(qs, q)
	}
	if value = r.FormValue("revolving_utilization_lt"); value != "" {
		q = "loans.revolving_utilization < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("revolving_balance_gt"); value != "" {
		q = "loans.revolving_balance > " + value
		qs = append(qs, q)
	}
	if value = r.FormValue("revolving_balance_lt"); value != "" {
		q = "loans.revolving_balance <" + value
		qs = append(qs, q)
	}

	if value = r.FormValue("annual_income_gt"); value != "" {
		q = "loans.annual_income > " + value
		qs = append(qs, q)
	}
	if value = r.FormValue("annual_income_lt"); value != "" {
		q = "loans.annual_income < " + value
		qs = append(qs, q)
	}

	// build WHERE conditions
	for i, el := range qs {
		if i == 0 {
			notesWhere = el
		} else {
			notesWhere = notesWhere + " AND " + el
		}
	}

	fmt.Println(notesWhere)

	// Query Execution
	rows, err := server.db.
		Table("notes").
		Select(`notes.id,
			notes.loan_id,
			notes.note_id,
			notes.order_id,
			notes.interest_rate,
			loans.class,
			notes.status,
			notes.ask_price,
			notes.ytm,
			notes.never_late,
			notes.amount,
			notes.discount,
			notes.outstanding_principal,
			notes.credit_score_trend,
			loans.dti,
			loans.annual_income,
			loans.term,
			loans.revolving_utilization,
			loans.revolving_balance`).
		Joins("left join loans on loans.loan_id = notes.loan_id").
		Where(notesWhere).
		Limit(250).
		Rows()

	var results []models.Note

	for rows.Next() {
		var note models.Note

		rows.Scan(
			&note.Id,
			&note.LoanId,
			&note.NoteId,
			&note.OrderId,
			&note.InterestRate,
			&note.Loan.Class,
			&note.Status,
			&note.AskPrice,
			&note.Ytm,
			&note.NeverLate,
			&note.Amount,
			&note.Discount,
			&note.OutstandingPrincipal,
			&note.CreditScoreTrend,
			&note.Loan.Dti,
			&note.Loan.AnnualIncome,
			&note.Loan.Term,
			&note.Loan.RevolvingUtilization,
			&note.Loan.RevolvingBalance,
		)

		results = append(results, note)
	}

	w.Header().Set("Content-Type", "application/json")
	j, err := json.Marshal(results)
	CheckErr(err)
	w.Write(j)
}

func (s *Server) apiLoansSearch(w http.ResponseWriter, r *http.Request) {

	r.ParseForm()
	var queryWhere string
	var value string
	var q string
	var qs []string

	if value = r.FormValue("interest_rate_gt"); value != "" {
		q = "interest_rate > " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("interest_rate_lt"); value != "" {
		q = "interest_rate < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("amount_gt"); value != "" {
		q = "amount > " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("amount_lt"); value != "" {
		q = "amount < " + value
		qs = append(qs, q)
	}

	if value = r.FormValue("loan_class"); value != "" {
		q = "loan_class in ("
		loan_classes := strings.Split(value, ",")

		for i, c := range loan_classes {
			q = q + "'" + strings.ToUpper(c) + "'"
			if i != len(loan_classes)-1 {
				q = q + ","
			}
		}
		q = q + ")"
		qs = append(qs, q)
	}

	for i, el := range qs {
		if i == 0 {
			queryWhere = el
		} else {
			queryWhere = queryWhere + " AND " + el
		}
	}

	fmt.Println(queryWhere)

	// Query Execution
	rows, err := s.db.
		Table("loans").
		Select(`loans.id,
		  loans.loan_id,
			loans.class,
			loans.dti,
			loans.annual_income,
			loans.term,
			loans.interest_rate,
			loans.amount,
			loans.revolving_utilization,
			loans.revolving_balance`).
		Where(queryWhere).
		Limit(250).
		Rows()

	var loans []models.Loan

	for rows.Next() {
		var loan models.Loan

		rows.Scan(
			&loan.Id,
			&loan.LoanId,
			&loan.Class,
			&loan.Dti,
			&loan.AnnualIncome,
			&loan.Term,
			&loan.InterestRate,
			&loan.Amount,
			&loan.RevolvingUtilization,
			&loan.RevolvingBalance,
		)

		loans = append(loans, loan)
	}

	w.Header().Set("Content-Type", "application/json")
	j, err := json.Marshal(loans)
	CheckErr(err)
	w.Write(j)
}
