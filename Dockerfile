FROM        google/debian:wheezy
MAINTAINER  Weston Platter <westonplatter@gmail.com>

ADD         fi fi

ENV PORT    8080
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD password
ENV APP_ENV production

ENTRYPOINT  ["/fi"]

