package models

import (
	// "encoding/csv"
	// "fmt"
	// sql "github.com/aodin/aspect"
	// _ "github.com/aodin/aspect/postgres"
	// "github.com/gorilla/mux"
	// "io"
	// "net/http"
	// "os"
	"time"
)

type User struct {
	Id        int64
	Birthday  time.Time
	Age       int64
	Name      string `sql:"size:255"`
	CreatedAt time.Time
	UpdatedAt time.Time
	DeletedAt time.Time
}
