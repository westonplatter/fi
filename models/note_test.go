package models

import (
	"encoding/csv"
	"fmt"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestInitCSV(t *testing.T) {
	assert := assert.New(t)

	r := csv.NewReader(noteCsv)
	lines, err := r.ReadAll()
	if err != nil {
		fmt.Println(err)
	}

	csvHeaders := lines[0]
	csvLine := lines[1]
	note := &Note{}

	note.CsvInit(csvHeaders, csvLine)

	assert.Equal(note.LoanId, 1507606)
	assert.Equal(note.NoteId, 13471609)
	assert.Equal(note.OrderId, 3271717)
	assert.Equal(note.OutstandingPrincipal, 0.02)
	assert.Equal(note.AccruedInterest, 0.1)
	assert.Equal(note.Status, "Current")
	assert.Equal(note.AskPrice, 0.03)
	assert.Equal(note.Discount, 2.03)
	assert.Equal(note.Ytm, 0.01)
	assert.Equal(note.Dslp, 25)
	assert.Equal(note.CreditScoreTrend, "DOWN")
	assert.Equal(note.FicoEndRange, "690-694")

	tt, _ := time.Parse("01/02/2006", "11/17/2014")
	assert.Equal(note.DatetimeListed, tt)

	assert.Equal(note.NeverLate, true)
	assert.Equal(note.LoanClass, "B")
	assert.Equal(note.LoanSubClass, "3")
	assert.Equal(note.Maturity, 36)
	assert.Equal(note.Amount, 25.0)
	assert.Equal(note.InterestRate, 12.12)
	assert.Equal(note.RemainingPayments, 1)
	assert.Equal(note.PrincipalInterest, 0.06)
}

var noteCsv = strings.NewReader(`
"LoanId","NoteId","OrderId","OutstandingPrincipal","AccruedInterest","Status","AskPrice","Markup/Discount","YTM","DaysSinceLastPayment","CreditScoreTrend","FICO End Range","Date/Time Listed","NeverLate","Loan Class","Loan Maturity","Original Note Amount","Interest Rate","Remaining Payments","Principal + Interest"
"1507606","13471609","3271717","0.02","0.1","Current","0.03","2.03","0.01","25","DOWN","690-694","11/17/2014","true","B3","36","25.0","12.12","1","0.06"
"969660","6581674","1881243","0.13","0.0","Current","0.13","0.00","0.01","20","DOWN","590-594","11/16/2014","false","B4","36","25.0","12.42","1","0.13"
"978812","6647898","1899086","0.19","0.0","Late (16-30 days)","0.17","-10.53","423.53","54","UP","800-804","11/17/2014","false","A2","36","25.0","6.62","1","0.19"
`)
