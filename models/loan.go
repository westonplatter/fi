package models

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
	//"time"
)

type Loan struct {
	Id                   int64   `json:"id"`
	LoanId               int64   `json:"loan_id"`
	Notes                []Note  `json:"notes"`
	MemberId             int64   `json:"member_id"`
	Amount               float64 `json:"amount"`
	FundedAmount         float64 `json:"funded_amount"`
	FundedAmountInvested float64 `json:"funded_amount_invested"`
	Term                 int64   `json:"term"`
	InterestRate         float64 `json:"interest_rate"`
	Installment          float64 `json:"installment"`
	Class                string  `json:"loan_class"`
	SubClass             string  `json:"sub_class"`
	EmploymentTitle      string  `json:"employment_title"`
	EmploymentLength     string  `json:"employment_length"`
	HomeOwnership        string  `json:"home_ownership"`
	AnnualIncome         float64 `json:"annual_income"`
	IncomeVerified       bool    `json:"income_verified"`
	//IssueDate                  JSONTime  `json:"issue_date"`
	Status      string `json:"status"`
	PaymentPlan string `json:"payment_plan"`
	Url         string `json:"url"`
	// @todo fix this column
	Description         string  `json:"description", sql:"type:varchar(1000)"`
	Purpose             string  `json:"purpose", sql:"size: 4000"`
	Title               string  `json:"title"`
	ZipCode             string  `json:"zip_code"`
	AddressState        string  `json:"address_state"`
	Dti                 float64 `json:"dti"`
	Delinquencies2Years int64   `json:"delinquencies_2years"`
	//EarliestCreditLine         time.Time `json:"earliest_credit_line"`
	FicoRangeLow               int64   `json:"fico_range_low"`
	FicoRangeHigh              int64   `json:"fico_range_high"`
	InqLast6Months             int64   `json:"inq_last_6months"`
	MonthsSinceLastDelinquency int64   `json:"months_since_last_delinqency"`
	MonthsSinceLastRecord      int64   `json:"months_since_last_record"`
	OpenAccounts               int64   `json:"open_accounts"`
	PublicRecords              int64   `json:"public_records"`
	RevolvingBalance           float64 `json:"revolving_balance"`
	RevolvingUtilization       float64 `json:"revolving_utilization"`
	TotalAccounts              int64   `json:"total_accounts"`
	InitialListStatus          string  `json:"initial_list_status"`
	OutstandingPrincipal       float64 `json:"outstanding_principal"`
	OutstandingPrincipalInv    float64 `json:"outstanding_principal_inv"`
	TotalPayment               float64 `json:"total_payment"`
	TotalPaymentInv            float64 `json:"total_payment_inv"`
	TotalReceivedPrincipal     float64 `json:"total_received_principal"`
	TotalReceivedInterest      float64 `json:"total_received_interest"`
	TotalRecievedLateFee       float64 `json:"total_received_late_feed"`
	Recoveries                 float64 `json:"recoveries"`
	CollectionRecoveryFee      float64 `json:"collection_recovery_feed"`
	//LastPaymentDate            time.Time `json:"last_payment_date,omitempty"`
	LastPaymentAmount float64 `json:"last_payment_amount"`
	//NextPaymentDate            time.Time `json:"next_payment_date,omitempty"`
	//LastCreditPullDate         time.Time `json:"last_credit_pull_date,omitempty"`
	LastFicoRangeLow          int64 `json:"last_fico_range_low"`
	LastFicoRangeHigh         int64 `json:"last_fico_range_high"`
	Collections12MonthsExMed  int64 `json:"collections_12_monthds_ex_med"`
	MonthsSinceLastMajorDerog int64 `json:"months_since_last_major_deorg"`
	PolicyCode                int64 `json:"policy_code"`
}

func (l *Loan) CsvInit(headers []string, data []string) {

	var err error
	var i int

	if len(data[0]) > 16 {
		fmt.Println("hi there")
		return
	}

	// Loan Id
	i = loanMapHeader[headers[0]]
	l.LoanId = getInt(data, i)

	// Member Id
	i = loanMapHeader[headers[1]]
	l.MemberId = getInt(data, i)

	// Amount
	i = loanMapHeader[headers[2]]
	l.Amount = getFloat(data, i)

	// Funded Amount
	i = loanMapHeader[headers[3]]
	l.FundedAmount = getFloat(data, i)

	// Funded Amount Invested
	i = loanMapHeader[headers[4]]
	l.FundedAmountInvested = getFloat(data, i)

	// Term
	i = loanMapHeader[headers[5]]
	termRegex := regexp.MustCompile(`[0-9]`)
	termMatches := termRegex.FindAllString(data[i], -1)
	termString := strings.Join(termMatches, "")
	l.Term, err = strconv.ParseInt(termString, 0, 64)
	checkErr(err)

	// Interest Rate
	i = loanMapHeader[headers[6]]
	l.InterestRate = extractDecimal(data, i)

	// Installment
	i = loanMapHeader[headers[7]]
	l.Installment = getFloat(data, i)

	// Class
	i = loanMapHeader[headers[8]]
	l.Class = data[i]

	// SubClass
	i = loanMapHeader[headers[9]]
	scRegex := regexp.MustCompile(`[0-9]`)
	l.SubClass = scRegex.FindString(data[i])

	// Employment Title
	i = loanMapHeader[headers[10]]
	l.EmploymentTitle = data[i]

	// Employment Length
	i = loanMapHeader[headers[11]]
	l.EmploymentLength = data[i]

	// Home Ownership
	i = loanMapHeader[headers[12]]
	l.HomeOwnership = data[i]

	// Annual Income
	i = loanMapHeader[headers[13]]
	l.AnnualIncome = getFloat(data, i)

	// Is Income Verified
	i = loanMapHeader[headers[14]]
	if data[i] == "Verified" {
		l.IncomeVerified = true
	} else {
		l.IncomeVerified = false
	}

	// Issue Date
	// i = loanMapHeader[headers[15]]
	// shortForm := "Jan-2006"
	// ww, err := time.Parse(shortForm, data[i])
	// l.IssueDate = JSONTime(time.Now())
	// checkErr(err)

	// Status
	i = loanMapHeader[headers[16]]
	l.Status = data[i]

	// Payment Plan
	i = loanMapHeader[headers[17]]
	l.PaymentPlan = data[i]

	// Url
	i = loanMapHeader[headers[18]]
	l.Url = data[i]

	// Description
	//i = loanMapHeader[headers[19]]
	//l.Description = strings.TrimSpace(data[i])

	// Purpose
	i = loanMapHeader[headers[20]]
	l.Purpose = data[i]

	// Title
	i = loanMapHeader[headers[21]]
	l.Title = data[i]

	// ZipCode
	i = loanMapHeader[headers[22]]
	l.ZipCode = data[i]

	// Address State
	i = loanMapHeader[headers[23]]
	l.AddressState = data[i]

	// Debt to Income Ratio
	i = loanMapHeader[headers[24]]
	l.Dti = getFloat(data, i)

	// Deliquencies 2 Years
	i = loanMapHeader[headers[25]]
	l.Delinquencies2Years = getInt(data, i)

	// EarliestCreditLine
	//i = loanMapHeader[headers[26]]
	//l.EarliestCreditLine, err = time.Parse("Jan-2006", data[i])
	//checkErr(err)

	// Fico End Range
	i = loanMapHeader[headers[27]]
	l.FicoRangeLow = getInt(data, i)

	// Fico End Range High
	i = loanMapHeader[headers[28]]
	l.FicoRangeHigh = getInt(data, i)

	// InqLast6Months
	i = loanMapHeader[headers[29]]
	l.InqLast6Months = getInt(data, i)

	// MonthsSinceLastDelinquency
	i = loanMapHeader[headers[30]]
	l.MonthsSinceLastDelinquency = getInt(data, i)

	// Months Since Last Record
	i = loanMapHeader[headers[31]]
	l.MonthsSinceLastRecord = getInt(data, i)

	// OpenAccounts
	i = loanMapHeader[headers[32]]
	l.OpenAccounts = getInt(data, i)

	// PublicRecords
	i = loanMapHeader[headers[33]]
	l.PublicRecords = getInt(data, i)

	// RevolvingBalance
	i = loanMapHeader[headers[34]]
	l.RevolvingBalance = getFloat(data, i)

	// RevolvingUtilization
	i = loanMapHeader[headers[35]]
	l.RevolvingUtilization = extractDecimal(data, i)

	// TotalAccounts
	i = loanMapHeader[headers[36]]
	l.TotalAccounts = getInt(data, i)

	// InitialListStatus
	i = loanMapHeader[headers[37]]
	l.InitialListStatus = data[i]

	// OutstandingPrincipal
	i = loanMapHeader[headers[38]]
	l.OutstandingPrincipal = getFloat(data, i)

	// OutstandingPrincipalInv
	i = loanMapHeader[headers[39]]
	l.OutstandingPrincipalInv = getFloat(data, i)

	// TotalPayment
	i = loanMapHeader[headers[40]]
	l.TotalPayment = getFloat(data, i)

	// TotalPaymentInv
	i = loanMapHeader[headers[41]]
	l.TotalPaymentInv = getFloat(data, i)

	// TotalReceivedPrincipal
	i = loanMapHeader[headers[42]]
	l.TotalReceivedPrincipal = getFloat(data, i)

	// TotalReceivedInterest
	i = loanMapHeader[headers[43]]
	l.TotalReceivedInterest = getFloat(data, i)

	// TotalRecievedLateFee
	i = loanMapHeader[headers[44]]
	l.TotalRecievedLateFee = getFloat(data, i)

	// Recoveries
	i = loanMapHeader[headers[45]]
	l.Recoveries = getFloat(data, i)

	// CollectionRecoveryFee
	i = loanMapHeader[headers[46]]
	l.CollectionRecoveryFee = getFloat(data, i)

	// LastPaymentDate
	//i = loanMapHeader[headers[47]]
	//l.LastPaymentDate, err = time.Parse("Jan-2006", data[i])
	//checkErr(err)

	// LastPaymentAmount
	i = loanMapHeader[headers[48]]
	l.LastPaymentAmount = getFloat(data, i)

	// NextPaymentDate
	//i = loanMapHeader[headers[49]]
	//if dstring := data[i]; dstring != "" {
	//l.NextPaymentDate, err = time.Parse("Jan-2006", data[i])
	//checkErr(err)
	//}

	// LastCreditPullDate
	//i = loanMapHeader[headers[50]]
	//if data[i] != "" {
	//l.LastCreditPullDate, err = time.Parse("Jan-2006", data[i])
	//checkErr(err)
	//}

	// LastFicoRangeLow
	i = loanMapHeader[headers[51]]
	l.LastFicoRangeLow = getInt(data, i)

	// LastFicoRangeHigh
	i = loanMapHeader[headers[52]]
	l.LastFicoRangeHigh = getInt(data, i)

	// Collections12MonthsExMed
	i = loanMapHeader[headers[53]]
	l.Collections12MonthsExMed = getInt(data, i)

	// MonthsSinceLastMajorDerog
	i = loanMapHeader[headers[54]]
	l.MonthsSinceLastMajorDerog = getInt(data, i)

	// PolicyCode
	i = loanMapHeader[headers[55]]
	if len(data[i]) == 1 {
		l.PolicyCode = getInt(data, i)
	}
}

func getFloat(data []string, i int) float64 {
	r, err := strconv.ParseFloat(data[i], 64)
	checkErr(err)
	return r
}

func getInt(data []string, i int) int64 {
	if data[i] == "" {
		return 0
	}
	r, err := strconv.ParseInt(data[i], 0, 64)
	checkErr(err)
	return r
}

func extractDecimal(data []string, i int) float64 {
	re := regexp.MustCompile(`[0-9|\.]`)
	matches := re.FindAllString(data[i], -1)
	str := strings.Join(matches, "")
	r, err := strconv.ParseFloat(str, 64)
	checkErr(err)
	return r
}

var loanMapHeader = map[string]int{
	"id":                          0,
	"member_id":                   1,
	"loan_amnt":                   2,
	"funded_amnt":                 3,
	"funded_amnt_inv":             4,
	"term":                        5,
	"int_rate":                    6,
	"installment":                 7,
	"grade":                       8,
	"sub_grade":                   9,
	"emp_title":                   10,
	"emp_length":                  11,
	"home_ownership":              12,
	"annual_inc":                  13,
	"is_inc_v":                    14,
	"issue_d":                     15,
	"loan_status":                 16,
	"pymnt_plan":                  17,
	"url":                         18,
	"desc":                        19,
	"purpose":                     20,
	"title":                       21,
	"zip_code":                    22,
	"addr_state":                  23,
	"dti":                         24,
	"delinq_2yrs":                 25,
	"earliest_cr_line":            26,
	"fico_range_low":              27,
	"fico_range_high":             28,
	"inq_last_6mths":              29,
	"mths_since_last_delinq":      30,
	"mths_since_last_record":      31,
	"open_acc":                    32,
	"pub_rec":                     33,
	"revol_bal":                   34,
	"revol_util":                  35,
	"total_acc":                   36,
	"initial_list_status":         37,
	"out_prncp":                   38,
	"out_prncp_inv":               39,
	"total_pymnt":                 40,
	"total_pymnt_inv":             41,
	"total_rec_prncp":             42,
	"total_rec_int":               43,
	"total_rec_late_fee":          44,
	"recoveries":                  45,
	"collection_recovery_fee":     46,
	"last_pymnt_d":                47,
	"last_pymnt_amnt":             48,
	"next_pymnt_d":                49,
	"last_credit_pull_d":          50,
	"last_fico_range_high":        52,
	"last_fico_range_low":         51,
	"collections_12_mths_ex_med":  53,
	"mths_since_last_major_derog": 54,
	"policy_code":                 55,
}
