package models

import (
	"encoding/csv"
	"os"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestLoan(t *testing.T) {
	assert := assert.New(t)

	file, _ := os.Open("../test/LCHistorical.csv")
	defer file.Close()
	reader := csv.NewReader(file)
	reader.FieldsPerRecord = -1
	lines, _ := reader.ReadAll()

	headers := lines[1]
	data := lines[2]

	loan := &Loan{}
	loan.CsvInit(headers, data)

	assert.Equal(loan.LoanId, 1077501)
	assert.Equal(loan.MemberId, 1296599)
	assert.Equal(loan.Amount, 5000.0)
	assert.Equal(loan.FundedAmount, 5001.0)
	assert.Equal(loan.FundedAmountInvested, 4975.0)
	assert.Equal(loan.Term, 36)
	assert.Equal(loan.InterestRate, 10.65)
	assert.Equal(loan.Installment, 162.87)
	assert.Equal(loan.Class, "B")
	assert.Equal(loan.SubClass, "2")
	assert.Equal(loan.EmploymentTitle, "programmer")
	assert.Equal(loan.EmploymentLength, "10+ years")
	assert.Equal(loan.IncomeVerified, true)
	assert.Equal(loan.Status, "Current")
	assert.Equal(loan.PaymentPlan, "n")
	assert.Equal(loan.Url, "https://www.lendingclub.com/browse/loanDetail.action?loan_id=1077501")
	assert.Equal(loan.Description, "Borrower added on 12/22/11 > I need to upgrade my business technologies.<br>")
	assert.Equal(loan.Purpose, "credit_card")
	assert.Equal(loan.Title, "Computer")
	assert.Equal(loan.ZipCode, "860xx")
	assert.Equal(loan.AddressState, "AZ")

	tt, _ := time.Parse("Jan-2006", "Dec-2011")
	assert.Equal(loan.IssueDate, tt)

	assert.Equal(loan.Delinquencies2Years, 9)

	tt, _ = time.Parse("Jan-2006", "Jan-1985")
	assert.Equal(tt, loan.EarliestCreditLine)

	assert.Equal(loan.Dti, 27.65)
	assert.Equal(loan.Delinquencies2Years, 9)

	tt, _ = time.Parse("Jan-2006", "Jan-1985")
	assert.Equal(loan.EarliestCreditLine, tt)

	assert.Equal(loan.FicoRangeLow, 735)
	assert.Equal(loan.FicoRangeHigh, 739)
	assert.Equal(loan.InqLast6Months, 55)
	assert.Equal(loan.MonthsSinceLastDelinquency, 66)
	assert.Equal(loan.MonthsSinceLastRecord, 77)
	assert.Equal(loan.OpenAccounts, 3)
	assert.Equal(loan.PublicRecords, 101)
	assert.Equal(loan.RevolvingBalance, 13648)
	assert.Equal(loan.RevolvingUtilization, 83.7)
	assert.Equal(loan.TotalAccounts, 9)
	assert.Equal(loan.InitialListStatus, "f")
	assert.Equal(loan.OutstandingPrincipal, 488.82)
	assert.Equal(loan.OutstandingPrincipalInv, 486.33)
	assert.Equal(loan.TotalPayment, 5363.71)
	assert.Equal(loan.TotalPaymentInv, 5336.95)
	assert.Equal(loan.TotalReceivedPrincipal, 4511.18)
	assert.Equal(loan.TotalReceivedInterest, 852.53)
	assert.Equal(loan.TotalRecievedLateFee, 18.70)
	assert.Equal(loan.Recoveries, 12.12)
	assert.Equal(loan.CollectionRecoveryFee, 25.13)

	tt, _ = time.Parse("Jan-2006", "Oct-2014")
	assert.Equal(loan.LastPaymentDate, tt)

	assert.Equal(loan.LastPaymentAmount, 162.87)

	tt, _ = time.Parse("Jan-2006", "Dec-2014")
	assert.Equal(loan.NextPaymentDate, tt)

	tt, _ = time.Parse("Jan-2006", "Oct-2014")
	assert.Equal(loan.LastCreditPullDate, tt)

	assert.Equal(loan.LastFicoRangeHigh, 734)
	assert.Equal(loan.LastFicoRangeLow, 730)
	assert.Equal(loan.MonthsSinceLastMajorDerog, 12)
	assert.Equal(loan.PolicyCode, 1)
}
