package models

import (
	"fmt"
	"runtime"
	"strconv"
	"time"
)

type Note struct {
	Id                   int64     `json:"id"`
	Loan                 Loan      `json:"loan"`
	NoteId               int64     `json:"note_id"`
	LoanId               int64     `json:"loan_id"`
	OrderId              int64     `json:"order_id"`
	OutstandingPrincipal float64   `json:"outstanding_principal"`
	AccruedInterest      float64   `json:"accured_interest"`
	Status               string    `json:"status"`
	AskPrice             float64   `json:"ask_price"`
	Ytm                  float64   `json:"ytm"`
	Discount             float64   `json:"discount"`
	Dslp                 int64     `json:"dslp"`
	CreditScoreTrend     string    `json:"credit_score_trend", sql:"size:255"`
	FicoEndRange         string    `json:"fico_end_range", sql:"size:255`
	DatetimeListed       time.Time `json:"omitempty", sql:"DEFAULT:null"`
	NeverLate            bool      `json:"never_late"`
	LoanClass            string    `json:"loan_class", sql:"size:255`
	LoanSubClass         string    `json:"loan_sub_class", sql:"size:255`
	Maturity             int64     `json:"maturity"`
	Amount               float64   `json:"amount"`
	InterestRate         float64   `json:"interest_rate"`
	RemainingPayments    int64     `json:"remaining_payments"`
	PrincipalInterest    float64   `json:"principal_interest"`
	//CreatedAt            time.Time `json:"omitempty"`
	//UpdatedAt            time.Time `json:"omitempty"`

	FiState int64 `json:"fi_state"`
	// 0 - untouched
	// 1 - updated
	MarketState int `json:"market_state"`
	// 0 - off market
	// 1 - on market

}

// initialize from csv line
func (n *Note) CsvInit(headers []string, data []string) {

	var err error
	var i int

	// LoanId
	i = mapHeader[headers[0]]
	n.LoanId, err = strconv.ParseInt(data[i], 0, 64)
	checkErr(err)

	// NoteId
	i = mapHeader[headers[1]]
	n.NoteId, err = strconv.ParseInt(data[i], 0, 64)
	checkErr(err)

	// OrderId
	i = mapHeader[headers[2]]
	n.OrderId, err = strconv.ParseInt(data[i], 0, 64)
	checkErr(err)

	// Outstanding Principal
	i = mapHeader[headers[3]]
	n.OutstandingPrincipal, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// Accured Interest
	i = mapHeader[headers[4]]
	n.AccruedInterest, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// Status
	i = mapHeader[headers[5]]
	n.Status = data[i]

	// Ask Price
	i = mapHeader[headers[6]]
	n.AskPrice, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// Discount
	i = mapHeader[headers[7]]
	n.Discount, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// YTM
	i = mapHeader[headers[8]]
	n.Ytm, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// Days Since Last Payment
	i = mapHeader[headers[9]]
	n.Dslp, err = strconv.ParseInt(data[i], 0, 64)
	checkErr(err)

	// Credit Score Trend
	// @todo break string into 2 columns and parse into ints
	i = mapHeader[headers[10]]
	n.CreditScoreTrend = data[i]

	// Fico End Range
	i = mapHeader[headers[11]]
	n.FicoEndRange = data[i]

	// Datetime Listed
	i = mapHeader[headers[12]]
	shortForm := "01/02/2006"
	n.DatetimeListed, err = time.Parse(shortForm, data[i])
	checkErr(err)

	// Never Late
	i = mapHeader[headers[13]]
	n.NeverLate, err = strconv.ParseBool(data[i])
	checkErr(err)

	// Loan Class
	i = mapHeader[headers[14]]
	n.LoanClass = string(data[i][0])
	n.LoanSubClass = string(data[i][1])

	// Maturity
	i = mapHeader[headers[15]]
	n.Maturity, err = strconv.ParseInt(data[i], 0, 64)
	checkErr(err)

	// Amount
	i = mapHeader[headers[16]]
	n.Amount, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// Interest Rate
	i = mapHeader[headers[17]]
	n.InterestRate, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)

	// Remaining Payments
	i = mapHeader[headers[18]]
	n.RemainingPayments, err = strconv.ParseInt(data[i], 0, 64)
	checkErr(err)

	// Principal and Interest
	i = mapHeader[headers[19]]
	n.PrincipalInterest, err = strconv.ParseFloat(data[i], 64)
	checkErr(err)
}

var mapHeader = map[string]int{
	"LoanId":               0,
	"NoteId":               1,
	"OrderId":              2,
	"OutstandingPrincipal": 3,
	"AccruedInterest":      4,
	"Status":               5,
	"AskPrice":             6,
	"Markup/Discount":      7,
	"YTM":                  8,
	"DaysSinceLastPayment": 9,
	"CreditScoreTrend":     10,
	"FICO End Range":       11,
	"Date/Time Listed":     12,
	"NeverLate":            13,
	"Loan Class":           14,
	"Loan Maturity":        15,
	"Original Note Amount": 16,
	"Interest Rate":        17,
	"Remaining Payments":   18,
	"Principal + Interest": 19,
}

func checkErr(err error) {

	if err != nil {
		_, _, lineNum, _ := runtime.Caller(2)
		fmt.Println()
		fmt.Println("Line Number = ")
		fmt.Println(lineNum)
		fmt.Println("Func caller = ")

		fmt.Println(err)
		return
	}
}
