package main

import (
	"fmt"
	"log"
	"net/http"
)

func Log(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Printf("%s %s %s", r.RemoteAddr, r.Method, r.URL)
		handler.ServeHTTP(w, r)
	})
}

func CheckErr(err error) {
	if err != nil {
		fmt.Println(err)
		return
	}
}

func PanicErr(err error) {
	if err != nil {
		panic(err)
	}
}
