package importers

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"

	models "bitbucket.org/westonplatter/fi/models"
)

func ImportLendingClubHistoricalLoanCSV(db gorm.DB, filename string) {
	fmt.Println("importing lending club historical data")

	file, err := os.Open("./tmp/" + filename)
	CheckErr(err)
	defer file.Close()

	reader := csv.NewReader(file)
	reader.FieldsPerRecord = -1

	rawCsvData, err := reader.ReadAll()
	CheckErr(err)

	headers := rawCsvData[1]

	totalLines := len(rawCsvData)

	for lineNumber, row := range rawCsvData[2:] {
		loanId, err := strconv.ParseInt(row[0], 0, 64)
		CheckErr(err)
		loan := models.Loan{LoanId: loanId}

		err = db.Where("loan_id =?", loanId).First(&loan).Error

		switch err {
		case gorm.RecordNotFound:
			loan.CsvInit(headers, row)
			db.Create(loan)
		case nil:
			updatedLoan := &models.Loan{LoanId: loanId}
			updatedLoan.CsvInit(headers, row)
			db.Model(&loan).Updates(updatedLoan)
		default:
			CheckErr(err)
		}
		if (lineNumber % 1000) == 0 {
			description := "Loan Import " + filename
			FmtProgress(description, lineNumber, totalLines)
		}
	}
}

func CheckErr(err error) {
	if err != nil {
		fmt.Println(err)
	}
}
