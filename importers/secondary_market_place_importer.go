package importers

import (
	"encoding/csv"
	"os"
	"strconv"

	"github.com/jinzhu/gorm"
	_ "github.com/lib/pq"

	models "bitbucket.org/westonplatter/fi/models"
)

func ImportSecondaryMarketPlaceCSV(db gorm.DB, filename string) {

	// open the csv file
	csvfile, err := os.Open("./tmp/" + filename)
	CheckErr(err)
	defer csvfile.Close()

	// update current records to before import state
	db.Exec("update notes set fi_state = 0 where market_state = 1")

	// read the file and load all rows
	// @todo consumer file contents in piecewise defined process
	reader := csv.NewReader(csvfile)
	reader.FieldsPerRecord = -1
	rawCSVdata, err := reader.ReadAll()
	CheckErr(err)

	// get the csv headers
	headers := rawCSVdata[0]

	totalLines := len(rawCSVdata)

	// iterate over the rows
	for lineNumber, row := range rawCSVdata[1:] {

		// initialize Note for query
		noteId, err := strconv.ParseInt(row[1], 0, 64)
		CheckErr(err)
		note := models.Note{NoteId: noteId}

		err = db.Where("note_id =?", noteId).First(&note).Error

		switch err {
		// create a new Note record
		case gorm.RecordNotFound:
			note.CsvInit(headers, row)
			note.MarketState = 1
			note.FiState = 1
			db.Create(note)

		// update alredy existing record
		case nil:
			updatedNote := &models.Note{NoteId: noteId}
			updatedNote.CsvInit(headers, row)
			updatedNote.MarketState = 1
			updatedNote.FiState = 1

			// @todo calculate diff between note and updatedNote
			db.Model(&note).Updates(updatedNote)

		// something else happened
		default:
			CheckErr(err)
		}

		if (lineNumber % 1000) == 0 {
			description := "Notes Import " + filename
			FmtProgress(description, lineNumber, totalLines)
		}
	}

	var nonUpdatedNotes []models.Note
	db.Where("fi_state = 0").Find(&nonUpdatedNotes)

	for _, note := range nonUpdatedNotes {
		// @todo make note of NoteSales
		note.MarketState = 0
		note.FiState = 1
		db.Model(&note).Updates(note)
	}

}
