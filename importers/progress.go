package importers

import (
	"fmt"
)

func FmtProgress(operation string, i int, total int) {

	ifloat := float64(i)
	totalfloat := float64(total)

	percent := (ifloat / totalfloat) * 100

	fmt.Println(fmt.Sprintf("============= %v. %4.2f", operation, percent))
}
